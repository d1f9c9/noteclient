import * as React from 'react';
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import FormatListNumberedIcon from '@mui/icons-material/FormatListNumbered';
import NoteAddIcon from '@mui/icons-material/NoteAdd';
import NotesIcon from '@mui/icons-material/Notes';
import NoteIcon from '@mui/icons-material/Note';

// @ts-ignore
import {useNavigate} from "react-router-dom";

export default function LabelBottomNavigation() {
    const [value, setValue] = React.useState('recents');

    const handleChange = (event: React.SyntheticEvent, newValue: string) => {
        setValue(newValue);
    };
    const navigate = useNavigate();

    return (
        <BottomNavigation className="side_menu" value={value} onChange={handleChange}>
            <BottomNavigationAction
                onClick={() => navigate('/note/add')}
                label="Создание плата на день"
                value="noteAddIcon"
                icon={<NoteAddIcon/>}
            />
            <BottomNavigationAction
                onClick={() => navigate('/note/list')}
                label="Список цедей на день"
                value="notesIcon"
                icon={<NotesIcon/>}
            />

            <BottomNavigationAction
                onClick={() => navigate('/purpose/add')}
                label="Создание глобальных целей"
                value="noteIcon"
                icon={<NoteIcon/>}
            />
            <BottomNavigationAction
                onClick={() => navigate('/purpose/list')}
                label="Список глобальных целей"
                value="favorites"
                icon={<FormatListNumberedIcon />}
            />
        </BottomNavigation>
    );
}
