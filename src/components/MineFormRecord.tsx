import React, {useEffect, useState} from 'react';
import {Button, DatePicker, Form, Input, Select, Switch, TimePicker,} from 'antd';
import { enumNameProps, IComplexity, IDirection, INote, IPurpose, IRecordLabel, NameOperation, TypeParent } from "../types/note";
import {noteApi} from "../store/note/note.api";
import {useSearchPriorityQuery} from "../store/priority/priority.api";
import '../style/FormAddRecordStyle.css';

const {RangePicker} = DatePicker;
const {TextArea} = Input;

interface IParams {
    record: IRecordLabel | undefined,
    complex: IComplexity[] | undefined,
    recNotePurpose: IPurpose | INote | undefined,
    shouldTo: IPurpose[] | IDirection[] | undefined,
    setRecNotePurpose: (value: IPurpose | INote) => void,
    typeParent: TypeParent,
    create: any
}

const MineFormRecord = (params: IParams) => {

    const {complex, record, shouldTo, recNotePurpose, setRecNotePurpose, create, typeParent} = params;
    const {data: priority} = useSearchPriorityQuery(null);

    const [note, setNote]: [INote | IPurpose, any] = useState(recNotePurpose!);
    const {data: defaultNotes} = noteApi.useSearchNoteDefaultQuery(null);
    const [addDefaultNote, {}]  = noteApi.useAddDefaultNoteMutation();

    useEffect(() => {
        console.log(recNotePurpose)
        setNote(recNotePurpose)
    }, [recNotePurpose])


    const actionCreateNote = async (note: INote | IPurpose) => {
        await create(note);
    }

    async function changeValue(key: string, value: unknown) {
        if (recNotePurpose) {
            await setNote({...note, [key]: value})
        }
        console.log(note, value)
    }

    async function changeArrayValues(key: string[], value: unknown[]) {
        if (recNotePurpose) {
            setNote({...note, [key[0]]: value[0], [key[1]]: value[1]})
        }
    }

    function changeCheckbox(key: string, value: any) {
        if (recNotePurpose) {
            if (enumNameProps.complexity === key && complex) {
                const complexity = complex.filter(val => val.id === value)[0];
                setNote({...note, complexity})
            } else if (enumNameProps.priority === key && priority) {
                const prioritys = priority.filter(val => val.id === value)[0];
                setNote({...note, priority: prioritys})
            } else if (enumNameProps.direction === key && shouldTo) {
                // @ts-ignore
                const direction = shouldTo.filter(val => val.id === value)[0];
                setNote({...note, direction})
            } else if (enumNameProps.purpose === key && shouldTo) {
                debugger
                // @ts-ignore
                const purpose = shouldTo.filter(val => val.id === value)[0];
                setNote({...note, purpose})
            }
        }
    }

    function toDateTypeRus(date: string) {
        const dateRev = date.split('-').reverse();

        return dateRev.join(':');
    }

    function convertDate (date: string) {
        const dateStart = date.slice(0, date.indexOf(' '));
        const dateEnd = date.slice(date.indexOf(' '));

        const string = 'Дата начала: ' + toDateTypeRus(dateStart) + ' --> ' + 'Deadline: ' + toDateTypeRus(dateEnd.trim());

        return string;
    }

    return (
        <div style={{marginLeft: '50px',  overflow: 'hidden'}}>
            {/*Основная инфомация*/}
            <h3 style={{width: '90%'}} className='head_label'>{record?.formName}</h3>
            <Form labelCol={{span: 5}} wrapperCol={{span: 14}}
                  layout='horizontal' disabled={false}>

                <h4 className='option_label'>{record?.mainParams}</h4>
                <Form.Item label={record!.name}>
                    <Input onChange={(event: any) => changeValue('name', event.target.value)}
                           value={note && note.name}/>
                </Form.Item>
                <Form.Item label={record!.results}>
                    <Input onChange={(event: any) => changeValue('results', event.target.value)}
                           value={note && note.results}/>
                </Form.Item>
            </Form>
            {/*Временные рамки*/}
            <div style={{marginLeft: '2%'}}>
                <h4 className='option_label'>Временные рамки</h4>
                <Form.Item>
                    {
                        record && record?.typeForm === NameOperation.purpose ?

                            <RangePicker
                                onChange={(value, dates) => changeArrayValues(['timeStart', 'deadline'], dates)}
                                style={{width: '25%'}}/>

                            :
                            <>
                                <DatePicker onChange={(value, data) => changeValue('dateStart', data)}
                                            style={{width: '25%', marginRight: '5%'}}/>
                                <TimePicker.RangePicker
                                    onChange={(value, dates) => changeArrayValues(['timeStart', 'deadline'], dates)}/>
                                {/*@ts-ignore*/}
                                <span  style={{width: '25%', marginLeft: '5%'}}>{recNotePurpose && recNotePurpose!.globalTime && convertDate(recNotePurpose!.globalTime)}</span>
                            </>
                    }
                </Form.Item>
                {/*Затрачено времяни',TODO*/}

                {/*Оценка задания*/}
                <div>
                    <h4 className='option_label'>{record?.taskEvaluation}</h4>
                    <div className={'row'}>
                        <Form.Item className='failed_size' label={record!.complexity}>
                            <Select defaultValue={note?.complexity?.id}
                                    onChange={value => changeCheckbox('complexity', value)}>
                                {complex && complex.map(complex =>
                                    <Select.Option value={complex.id}>{complex.name}</Select.Option>
                                )}
                            </Select>
                        </Form.Item>
                        <Form.Item className='failed_size' label={record!.priority}>
                            <Select defaultValue={note?.priority?.id}
                                    onChange={value => changeCheckbox('priority', value)}>
                                {priority && priority.map(priorit =>
                                    <Select.Option value={priorit.id}>{priorit.name}</Select.Option>
                                )}
                            </Select>
                        </Form.Item>
                        <Form.Item className='failed_size' label={record!.shouldTo}>
                            <Select onChange={value => changeCheckbox(typeParent, value)}>
                                {shouldTo && shouldTo.map(should =>
                                    <Select.Option value={should.id}>{should.name}</Select.Option>
                                )}
                            </Select>
                        </Form.Item>
                    </div>

                    {/*Условие выполнения*/}
                    <h4 className='option_label'>{record?.conditionDone}</h4>
                    <div className={'row'}>
                        <Form.Item style={{width: '40%'}} label={record!.isDone} valuePropName="checked">
                            <Switch defaultChecked={recNotePurpose?.isDone!} onChange={value => changeValue('isDone', value)}/>
                        </Form.Item>
                        {
                            record?.isDefault &&
                            <Form.Item style={{width: '40%'}} label={record!.isDefault} valuePropName="checked">
                                <Switch onChange={value => changeValue('isDefault', value)}/>
                            </Form.Item>
                        }
                        <Form.Item style={{width: '45%'}} label={record!.reasonsForDoing}>
                            <Input  style={{width: '90%'}} onChange={value => changeCheckbox('reasonsForDoing', value)}/>
                        </Form.Item>
                    </div>
                    {/*Приостановление выполнения*/}
                    <h4 className='option_label'>{record?.suspending}</h4>
                    <div className={'row'}>
                        <Form.Item style={{width: '29%', marginRight: '13%'}} label={record!.isSuspended}
                                   valuePropName="checked">
                            <Switch onChange={(val) => changeValue('isSuspended', val)}/>
                        </Form.Item>
                        <Form.Item style={{width: '40%'}} label={record!.timeSuspended}>
                            <DatePicker style={{width: '45%'}}
                                        onChange={(event, date) => changeValue('timeSuspended', date)}/>
                        </Form.Item>
                    </div>
                    {/*Удаление задания*/}
                    <h4 style={{margin: '1% 5% 0 1%', fontWeight: 'bold'}}>{record?.delete}</h4>
                    <div className={'row'}>
                        <Form.Item style={{width: '30%', marginRight: '13%'}} label={record!.isDeleted}
                                   valuePropName="checked">
                            <Switch onChange={(val) => changeValue('isDeleted', val)}/>
                        </Form.Item>
                        <Form.Item style={{width: '43%'}} label={record!.causeDeleted}>
                            <Input onChange={(event) => changeValue('causeDeleted', event.target.value)}/>
                        </Form.Item>
                    </div>

                    <Form.Item style={{position: "relative", left: '90%', marginTop: '6%', width: '5%'}}>
                        <Button onClick={() => actionCreateNote(note!)}>Сохранить</Button>
                    </Form.Item>
                    <Form.Item style={{position: "relative", left: '70%', width: '5%'}}>
                        <Button onClick={() => addDefaultNote(defaultNotes!)}>Добавить ежедневные задания</Button>
                    </Form.Item>
                </div>
            </div>
        </div>
    );
};

export default MineFormRecord;


//  const [recNotePurposePr, setRecNotePurposePr] = useState(recNotePurpose);

// const [componentDisabled, setComponentDisabled] = useState<boolean>(true);
/*    const onFormLayoutChange = ({disabled}: { disabled: boolean }) => {
    setComponentDisabled(disabled);
};*/