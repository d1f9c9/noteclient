import React, {useState} from "react";
import {Navigate} from "react-router-dom";
import {Route, Routes} from 'react-router-dom';
import {privateRouters, routers} from "../router";

const AppRoutes = function () {
    const [isAuth, setIsAuth] = useState(localStorage.getItem('isAuth'));

    return (
        <>
            {!isAuth ?
                <Routes>
                    {routers.map(({path, Element, exact}) =>
                        <>
                            <Route path={path} element={<Element/>}/>
                            <Route path="/" element={<Navigate to="/authorization"/>}/>
                        </>
                    )}
                </Routes>
                :
                <Routes>
                    {privateRouters.map(({path, Element, exact}) =>
                        <>
                            <Route path={path} element={<Element/>}/>
                            <Route path="/" element={<Navigate to="/authorization"/>}/>
                        </>
                    )}
                </Routes>}
        </>
    );
};

export default AppRoutes;