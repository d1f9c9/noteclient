import {Table} from 'antd';
import React, {useState} from 'react';
import {INameRecordLabel, INote, IPurpose, NameOperation} from "../types/note";
import {useSearchComplexityNoteQuery} from "../store/complexity/complexityNote.api";
import {useSearchPriorityQuery} from "../store/priority/priority.api";
import {CheckOutlined, CloseOutlined, DeleteOutlined, EditOutlined, FileDoneOutlined} from "@ant-design/icons";
import {useNavigate} from "react-router-dom";
import {noteApi, useSearchNoteQuery} from "../store/note/note.api";
import Loading from "../animation/bluePoint/Loading";

interface IParams {
    recordLabel: INameRecordLabel | undefined,
    optionName: NameOperation,
    data: INote[] | undefined,
    isLoading: boolean,
}

const ListAndFilter = (params: IParams) => {
    const {recordLabel, optionName, data, isLoading} = params;
    const navigate = useNavigate();
    let columns: any;

    const {data: complexityNote} = useSearchComplexityNoteQuery(null);
    const [createNote, {}] = noteApi.useNoteDoneMutation();
    const [deleteNote, {}] = noteApi.useNoteDeleteMutation();

    const {data: priority} = useSearchPriorityQuery(null);
    const [isLoad, setIsLoad] = useState(true);


    function getParamsFailed(column: any, param: string, failed: any) {
        column.filters = failed!.map((value: any) => {
            return {
                text: value.name,
                value: value.name
            }
        });
        // @ts-ignore
        column.sorter = (a: IPurpose, b: IPurpose) => a[param]?.name?.localeCompare(b[param]?.name ?? '');
    }

    function getNameString(key: string, property: string, column: any) {
        if (key === property) {
            column.dataIndex = 'shouldTo' === property ? 'purpose': property;
            column.render =(item: any) => item?.name ?? 'Нет данных';
            column.onFilter = (value: any, record: any) => record[property].name.startsWith(value);

            if (key === 'complexity' && complexityNote) {
                getParamsFailed(column, key, complexityNote);
            } else if (key === 'priority' && priority) {
                getParamsFailed(column, key, priority);
            } /*else if (key === 'shouldTo') {
                getParamsFailed(column, key, priority);
            }*/
        }

        return column
    }

    function toDateTypeRus(date: string) {
        const dateRev = date.split('-').reverse();

        return dateRev.join(':');
    }

    function convertDate (date: string) {
        const dateStart = date.slice(0, date.indexOf(' '));
        const dateEnd = date.slice(date.indexOf(' '));

        const string = toDateTypeRus(dateStart) + ' ' + toDateTypeRus(dateEnd.trim());

        return string;
    }



    if (recordLabel) {
        const getText = (item: any) => item && item;

        columns = Object.entries(recordLabel).map(([key, value]) => {
            const column = {
                title: value,
                dataIndex: key,
                render: (item: any) => {
                    console.log(key, item)
                    return key !== 'globalTime' ? <span style={{fontSize: '89%'}}>{getText(item)}</span> ?? 'Нет данных' :
                        <span style={{fontSize: '100%'}}>{getText(convertDate(item))}</span> ?? 'Нет данных'
                },
                filterMode: 'tree',
                width: '10%',
                filterSearch: true,
            } as any;
            const failedComplexity = ['complexity', 'priority', 'shouldTo'];
            failedComplexity.forEach(value => getNameString(key, value, column));

            if (!failedComplexity.includes(key)) {
                // @ts-ignore
                column.sorter = (a: IPurpose, b: IPurpose) => {
                    // @ts-ignore
                    if (a !== null && b !== null && typeof a[key] === 'string' && typeof b[key] === 'string')
                        // @ts-ignore
                        return a[key].localeCompare(b[key] ?? '');
                }
            }

            return column;
        })
    }

    const [edit, setEdit] = useState('');
    columns.push({
        title: 'Действие',
        dataIndex: 'id',
        render: (record: any) => {

            return (
                <>
                    <div className="flex">
                        <EditOutlined  // @ts-ignore
                            onClick={() => navigate(`/${optionName}/${record}`)}
                            className='icon_action background_red'/>
                        <FileDoneOutlined onClick={() => createNote(record)}
                            className='icon_action'/>
                        <DeleteOutlined onClick={() => deleteNote(record)}
                                         className='icon_action'/>
                    </div>
                </>
            );
        }
    });

    columns.push({
        title: 'Сделано',
        dataIndex: 'isDone',
        width: '1px',
        render: (record: any) => {
            return (
                <>
                    {record ? <CheckOutlined onClick={() => createNote(record)} /> :
                        <CloseOutlined onClick={() => createNote(record)} />}
                </>
            );
        }
    });




    setTimeout(() => setIsLoad(isLoading), 500);

    return !isLoad ? <Table style={{background: 'bisque', marginLeft: '70px'}} columns={columns}
                            dataSource={data} onChange={() => {
        }}/>
        :
        <Loading/>
};

export default ListAndFilter;