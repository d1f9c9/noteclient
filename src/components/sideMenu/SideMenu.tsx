import React, {useEffect, useState} from 'react';
import MenuItem from './MenuItem';
import './../../style/SideMinuStyle.css';

export const menuItems = [
    {
        name: 'Задачи на сегодня',
        exact: true,
        to: '/',
        id: '1',
    },
    {
        name: 'Создание задачи',
        exact: true,
        to: `/purps`,
        id: '2',
        iconClassName: 'bi bi-newspaper',
        subMenus: [
            {name: 'На день', id: '4', to: `/note/add`},
            {name: 'Глобальные', id: '5', to: `/purpose/add`}
        ]
    },
    {
        name: 'Списки',
        exact: true,
        to: 'note/today',
        id: '3',
        subMenus: [
            {name: 'Задача на завтра', to: '/note/tomorrow', id: '10'},
            {name: 'Глобальных задач', to: '/note/list', id: '6'},
            {name: 'На день', to: '/purpose/list', id: '7'},
            {name: 'На каждый день', to: '/note/default', id: '9'}
        ]
    },
    {
        name: 'Reference',
        to: `/reference`,
        id: '8',
    }
];

const SideMenu = (props: any) => {
    const [inactive, setInactive] = useState(false);
    const [menuFilter, setMenuFilter] = useState('');

    useEffect(() => {
        if (inactive) {
            removeActiveClassFromSubMenu();
        }

        //props.onCollapse(inactive);
    }, [inactive]);

    const removeActiveClassFromSubMenu = () => {
        document.querySelectorAll('.sub-menu').forEach((el) => {
            el.classList.remove('active');
        });
    };

    useEffect(() => {
        let menuItems = document.querySelectorAll('.menu-item');
        menuItems.forEach((el) => {
            el.addEventListener('click', (e) => {
                const next = el.nextElementSibling;
                removeActiveClassFromSubMenu();
                menuItems.forEach((el) => el.classList.remove('active'));
                el.classList.toggle('active');
                console.log(next);
                if (next !== null) {
                    next.classList.toggle('active');
                }
            });
        });
    }, []);

    return (
        <div onMouseLeave={() => {
            setInactive(true)
        }} onMouseOver={() => {
            setInactive(false)
        }} className={`side-menu ${inactive ? 'inactive' : ''}`}>
            <div className='top-section'>
                <div className='logo'>
                    {/*    <img src={''} alt='MyLogo' />*/}
                </div>
                <div onClick={() => setInactive(!inactive)} className='toggle-menu-btn'>
                    {inactive ? (
                        <i className='bi bi-arrow-right-square-fill'></i>
                    ) : (
                        <i className='bi bi-arrow-left-square-fill'></i>
                    )}
                </div>
            </div>
            <div className='divider'></div>
            <div className='main-menu'>
                <ul>
                    {menuItems.map((menuItems) => (
                        <MenuItem
                            key={menuItems.id}
                            name={menuItems.name}
                            to={menuItems.to}
                            subMenus={menuItems.subMenus || []}
                            onClick={() => {
                                if (true) {
                                    //  setInactive(true);
                                }
                            }}
                        />
                    ))}
                </ul>
            </div>
            <div className='side-menu-footer'>
                <div className='avatar'>
                    {/*    ///<img src={''} alt='user' />*/}
                </div>
                <div className='user-info'>
                    <h5>User Name</h5>
                    <p>firstName.gmail.com</p>
                </div>
            </div>
        </div>
    );
};

export default SideMenu;
