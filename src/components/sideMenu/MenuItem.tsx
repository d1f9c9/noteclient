import React, { useState } from 'react';
import { NavLink, Link } from 'react-router-dom';
import './../../style/SideMinuStyle.css';

const MenuItem = (props: any) => {
    const { name, id, subMenus, iconClassName, onClick, to, exact } = props;
    const [expand, setExpand] = useState(false);

    // @ts-ignore
    return (
        <li onClick={props.onClick}>
            <NavLink
    to={to}
    onClick={() => setExpand(!expand)}
    className="menu-item"
    //@ts-ignore

    >
    <div className="menu-icon">
    <i className={iconClassName}></i>
        </div>
        <span>{name}</span>
        </NavLink>
    {subMenus && subMenus.length > 0 ? (
        <ul className={`sub-menu ${expand ? "active" : ""}`}>
        {subMenus.map((menu:any) => (
            <li key={menu.id}>
            <NavLink to={menu.to}>{menu.name}</NavLink>
                </li>
        ))}
        </ul>
    ) : null}
    </li>
);
};

export default MenuItem;
