export let lists: string;

/*
import {AppDispatch} from "../index";
import axios from "axios";
import {IComplexity} from "../../types/note";
import {complexitySlice} from "./complexitySlice";
import {createAsyncThunk} from "@reduxjs/toolkit";

/!*
export const fetchComplexity = () => async (dispatch: AppDispatch) => {
    try {
        dispatch(complexitySlice.actions.complexityFetching())
        const response = await axios.get<IComplexity[]>('http://localhost:8055/complexity/note');
        dispatch(complexitySlice.actions.complexityFetchingSuccess(response.data))
    } catch (e) {
        dispatch(complexitySlice.actions.complexityFetchingError('Ошибка загрузки уровня сложности!!'));
        console.log(e);
    }
}
*!/

export const fetchComplexity = createAsyncThunk(
    'complexity/fetchAll',
    async (_, thunkAPI) => {
        try {
            const response = await axios.get<IComplexity[]>('http://localhost:8055/complexity/note');
            return response.data;

        } catch (e) {
            return thunkAPI.rejectWithValue('Ошибка при загрузки уровней сложности!!')
        }
    })
*/
