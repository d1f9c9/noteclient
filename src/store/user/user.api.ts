import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {IUser} from "../../types/user";

export const user = createApi({
    reducerPath: 'user/api',
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:8055',
    }),
    endpoints: build => ({
        addUser: build.mutation<IUser, IUser>({
            query: (user: IUser) => ({
                url: 'user/registration',
                method: 'POST',
                body: user
            })
        }),
        authUser: build.mutation<boolean, IUser>({
            query: (user: IUser) => ({
                url: 'user/authorization',
                method: 'POST',
                body: user
            })
        }),

    })
});

export const {useAddUserMutation, useAuthUserMutation} = user;