import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";

export const directionType = createApi({
    reducerPath: 'directionType/api',
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:8055',
    }),
    endpoints: build => ({
        searchDirectionType: build.query<any, any>({
            query: (search: any) => ({
                url: 'direction/type',
            })
        })
    })
});

export const {useSearchDirectionTypeQuery} = directionType;