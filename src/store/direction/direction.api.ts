import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {IDirection} from "../../types/note";

export const direction = createApi({
    reducerPath: 'direction/api',
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:8055',
    }),
    endpoints: build => ({
        searchDirection: build.query<IDirection[], any>({
            query: (search: any) => ({
                url: 'direction',
            })
        })
    })
});

export const {useSearchDirectionQuery} = direction;