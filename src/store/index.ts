import {combineReducers, configureStore} from "@reduxjs/toolkit";
import {complexityNote} from "./complexity/complexityNote.api";
import {directionType} from "./typeDirection/typeDirection.api";
import {complexityPurpose} from "./complexity/complexityPurpose.api";
import {direction} from "./direction/direction.api";
import {noteApi} from "./note/note.api";
import {purposeApi} from "./purpose/purpose.api";
import {priority} from "./priority/priority.api";


const rootReducer = combineReducers({

});


export const store = configureStore({
    reducer: {
        [complexityNote.reducerPath]: complexityNote.reducer,
        [complexityPurpose.reducerPath]: complexityPurpose.reducer,
        [directionType.reducerPath]: directionType.reducer,
        [direction.reducerPath]: direction.reducer,
        [noteApi.reducerPath]: noteApi.reducer,
        [priority.reducerPath]: priority.reducer,
        [purposeApi.reducerPath]: purposeApi.reducer,
    }
});