import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";

export const complexityPurpose = createApi({
    reducerPath: 'complexityPurpose/api',
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:8055',
    }),
    endpoints: build => ({
        searchComplexityPurpose: build.query<any, any>({
            query: (search: any) => ({
                url: 'complexity/purpose',
            })
        })
    })
});

export const {useSearchComplexityPurposeQuery} = complexityPurpose;