import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {createEntityAdapter, createSlice} from "@reduxjs/toolkit";
import {IComplexity} from "../../types/note";

export const complexityNote = createApi({
    reducerPath: 'complexityNote/api',
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:8055',
    }),
    endpoints: build => ({
        searchComplexityNote: build.query<IComplexity[], any>({
            query: (search: any) => ({
                url: 'complexity/note',
            })
        })
    })
});

const booksAdapter = createEntityAdapter({
    // Сортируем массив с идентификаторами по заголовкам книг
   // sortComparer: (a, b) => a.title.localeCompare(b.title),
});

const booksSlice = createSlice({
    name: 'books',
    initialState: booksAdapter.getInitialState({
        loading: 'idle',
    }),
    reducers: {
        bookAdded: booksAdapter.addOne,
        booksLoading(state, action) {
            if (state.loading === 'idle') {
                state.loading = 'pending';
            }
        },
        booksReceived(state, action) {
            if (state.loading === 'pending') {
                booksAdapter.setAll(state, action.payload);
                state.loading = 'idle';
            }
        },
        bookUpdated: booksAdapter.updateOne,
    },
});

export const {useSearchComplexityNoteQuery} = complexityNote;