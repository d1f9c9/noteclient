import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {INote, IPurpose} from "../../types/note";

export const purposeApi = createApi({
    reducerPath: 'purpose/api',
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:8055',
    }),
    endpoints: build => ({
        searchPurpose: build.query<any, any>({
            query: (search: any) => ({
                url: 'purpose',
            })
        }),
        searchPurposeById: build.query<IPurpose, number>({
            query: (search: number) => ({
                url: `purpose/${search}/`,
            })
        }),
        createPurpose: build.mutation<IPurpose, IPurpose> ({
            query: (purpose: IPurpose) => ({
                url: 'purpose',
                method: 'POST',
                body: purpose
            })
        })
    })
});

export const {useSearchPurposeQuery} = purposeApi;