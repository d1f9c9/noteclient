import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {IPriority} from "../../types/note";

export const priority = createApi({
    reducerPath: 'priority/api',
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:8055',
    }),
    endpoints: build => ({
        searchPriority: build.query<IPriority[], null>({
            query: (search: any) => ({
                url: 'priority',
            })
        })
    })
});

export const {useSearchPriorityQuery} = priority;