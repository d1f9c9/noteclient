import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {IFilterNote, INote} from "../../types/note";

export const noteApi = createApi({
    reducerPath: 'note/api',
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:8055',
    }),
    endpoints: build => ({
        searchNote: build.query<any, any>({
            query: (search: any) => ({
                url: 'note',
            })
        }),
        searchNoteById: build.query<INote, number>({
            query: (search: number) => ({
                url: `note/${search}/`,
            })
        }),
        searchNoteDefault: build.query<INote[], null>({
            query: (search: null) => ({
                url: 'note/default',
            })
        }),
        searchNoteToday: build.query<INote[], null>({
            query: (search: null) => ({
                url: 'note/today',
            })
        }),
        searchNoteTomorrow: build.query<INote[], null>({
            query: (search: null) => ({
                url: 'note/tomorrow',
            })
        }),
        createNote: build.mutation<INote, INote>({
            query: (note: INote) => ({
                url: 'note',
                method: 'POST',
                body: note,
                headers: {
                    ContentType: 'application/json; charset=utf-8'
                }
            })
        }),
        addDefaultNote: build.mutation<null, INote[]>({
            query: (notes: INote[]) => ({
                url: 'note/add/default',
                method: 'POST',
                body: notes,
                headers: {
                    ContentType: 'application/json; charset=utf-8'
                }
            })
        }),
        searchNoteByFilter: build.query<INote[], IFilterNote>({
            query: (note: IFilterNote) => ({
                url: 'note/filter',
                method: 'POST',
                body: note,
                headers: {
                    ContentType: 'application/json; charset=utf-8'
                }
            })
        }),
        updateNote: build.mutation<INote, INote>({
            query: (note: INote) => ({
                url: 'note',
                method: 'PUT',
                body: note,
                headers: {
                    ContentType: 'application/json; charset=utf-8'
                }
            })
        }),
        noteDone: build.mutation<string, string>({
            query: (noteId: string) => ({
                url: `note/done/${noteId}/`,
                method: 'PUT',
                headers: {
                    ContentType: 'application/json; charset=utf-8'
                }
            })
        }),
        noteDelete: build.mutation<string, string>({
            query: (noteId: string) => ({
                url: `note/${noteId}/`,
                method: 'DELETE',
                headers: {
                    ContentType: 'application/json; charset=utf-8'
                }
            })
        })
    }),
});

export const {useSearchNoteQuery} = noteApi;