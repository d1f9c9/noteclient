import React from 'react';
import './../style/BluePointerStyle.css'

const Loading = function () {
    return (
        <div className="Loading">
            <div className="bigCircle">
                <div className="circle">
                    <p>loading</p>
                </div>
                <p></p>
            </div>
        </div>
    );
};

export default Loading;