import React, {useState} from 'react';
import MineFormRecord from "../components/MineFormRecord";
import {FormName, IPurpose, IRecordLabel, NameNote, NameOperation, ShouldTo, TypeParent} from "../types/note";
import {useSearchDirectionQuery} from "../store/direction/direction.api";
import {purposeApi} from "../store/purpose/purpose.api";
import {useSearchComplexityPurposeQuery} from "../store/complexity/complexityPurpose.api";

const AddPurpose = function () {

    const purposeLabel: IRecordLabel = {
        typeForm: NameOperation.purpose,
        formName: FormName.purpose,
        mainParams: 'Основные параметры',
        taskEvaluation: 'Оценка задачи',
        extendedFor: 'Продлен для',
        conditionDone: 'Условия выполнения',
        suspending: 'Приостановление выполнения',
        delete: 'Удаление задания',
        name: NameNote.purpose,
        results: 'Необходимо достичь',
        complexity: 'Уровень сложности',
        priority: 'Уровень приоритета',
        shouldTo: ShouldTo.direction,
        timeStart: 'Время начала выполнения',
        deadline: 'Необходимо выполнить к дате',
        reasonsForDoing: 'Было достигнуто',
        isDone: 'Выполнено',
        extension: 'Задания',
        wastedTime: 'Затрачено времяни',
        isSuspended: 'Выполнение приостановлено',
        timeSuspended: 'Когда было приостановлено',
        isDeleted: 'Цель удалена',
        causeDeleted: 'Причина удаления',
    };

    const [purposeDefault, setPurpose]: [IPurpose, any] = useState({
        id: undefined,
        name: '',
        results: '',
        complexity: {
            id: 3,
            name: 'Средний',
            note: 'Выполнение задачи до 10 часов',
            code: 'AVERAGE'
        },
        priority: {
            id: 2,
            name: 'Средний',
            code: 'MEDIUM'
        },
        direction: null,
        timeStart: null,
        deadline: null,
        reasonsForDoing: null,
        isDone: false,
        wastedTime: null,
        isSuspended: false,
        timeSuspended: null,
        isDeleted: false,
        causeDeleted: null,
    });

    const {data: complexityPurpose} = useSearchComplexityPurposeQuery(null);
    const {data: direction} = useSearchDirectionQuery(null);
    const [createPurpose, {}] = purposeApi.useCreatePurposeMutation();

    return (
        <div>
            <MineFormRecord typeParent={TypeParent.direction} create={createPurpose} recNotePurpose={purposeDefault} record={purposeLabel}
                            complex={complexityPurpose} shouldTo={direction} setRecNotePurpose={setPurpose}/>
        </div>
    );
}

export default AddPurpose;


//  const dispatch = useAppDispatch();
// const {complexity, isLoading, error} = useAppSelector(state => state.complexityReducer);

/*
    useEffect(() => {
        dispatch(fetchComplexity())
    }, [])
*/