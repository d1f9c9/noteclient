import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import './../style/AuthtorizationForm.css'
import {useAddUserMutation, useAuthUserMutation} from "../store/user/user.api";
import {useState} from "react";
import {IUser} from "../types/user";

function AuthUser() {
    const [createUser, {}] = useAddUserMutation();
    const [authUser, {}] = useAuthUserMutation();

    const defaultUser: IUser = {
        id: null,
        login: null,
        password: null,
        mail: null,
    }
    const [user, setUser] = useState(defaultUser);

    function addUser(user: IUser | null) {
        if (user != null) {
            localStorage.setItem('isAuth', 'true');
            delete user.password
            localStorage.setItem('user', JSON.stringify(user));
        } else {
            localStorage.setItem('isAuth', '');
            localStorage.setItem('user', '');
        }

    }

    async function userInitialization(isAuth = true) {
        if(isAuth) {
            const newUser = await authUser(user) as unknown as IUser;
            addUser(newUser);
        } else {
            const newUser = await createUser(user) as unknown as IUser;
            addUser(newUser);
        }
    }

    return (
        <>
            <div className='center'>
                <div className='center_window'>
                    <h4><b>Форма пользователя</b></h4>
                    <Form>
                        <Form.Group onChange={(event: any) => setUser({...user, login: event.target.value})}
                                    className="mb-3"
                                    controlId="formBasicEmail">
                            <Form.Label>Логин</Form.Label>
                            <Form.Control type="email" placeholder="Логин"/>
                            <Form.Text className="text-muted">
                            </Form.Text>
                        </Form.Group>
                        <Form.Group onChange={(event: any) => setUser({...user, mail: event.target.value})}
                                    className="mb-3"
                                    controlId="formBasicEmail">
                            <Form.Label>Почта</Form.Label>
                            <Form.Control type="email" placeholder="Почта"/>
                            <Form.Text className="text-muted">
                            </Form.Text>
                        </Form.Group>
                        <Form.Group onChange={(event: any) => setUser({...user, password: event.target.value})}
                                    className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Пароль</Form.Label>
                            <Form.Control type="password" placeholder="Пароль"/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicCheckbox">

                        </Form.Group>
                        <Button style={{width: '400px', marginBottom: '20px'}} onClick={() => userInitialization(true)} variant="primary"
                                type="button">
                           Авторизация
                        </Button>
                        <Button style={{width: '400px'}} onClick={() => userInitialization(false)} variant="primary"
                                type="button">
                          Регистрация
                        </Button>
                    </Form>
                </div>
            </div>
        </>
    );
}

export default AuthUser;