import React, {useState} from 'react';
import MineFormRecord from "../components/MineFormRecord";
import {FormName, INote, IRecordLabel, NameNote, NameOperation, ShouldTo, TypeParent} from "../types/note";
import {useSearchComplexityNoteQuery} from "../store/complexity/complexityNote.api";
import {useSearchPurposeQuery} from "../store/purpose/purpose.api";
import {noteApi} from "../store/note/note.api";

const AddNote = function () {
    const noteLabel: IRecordLabel = {
        isDefault: 'Ежедневное задание',
        typeForm: NameOperation.note,
        formName: FormName.note,
        mainParams: 'Основные параметры',
        taskEvaluation: 'Оценка задачи',
        extendedFor: 'Продлен для',
        conditionDone: 'Условия выполнения',
        suspending: 'Приостановление выполнения',
        delete: 'Удаление задания',
        name: NameNote.note,
        results: 'Необходимо достичь',
        complexity: 'Уровень сложности',
        priority: 'Уровень приоритета',
        shouldTo: ShouldTo.purpose,
        timeStart: 'Время начала выполнения',
        deadline: 'Необходимо выполнить к дате',
        reasonsForDoing: 'Было достигнуто',
        isDone: 'Выполнено',
        extension: 'Задания',
        wastedTime: 'Затрачено времяни',
        isSuspended: 'Выполнение приостановлено',
        timeSuspended: 'Когда было приостановлено',
        isDeleted: 'Цель удалена',
        causeDeleted: 'Причина удаления',
    };

    const [noteDefault, setNote]: [INote, any] = useState({
        id: undefined,
        name: '',
        results: '',
        complexity: {
            id: 3,
            name: 'Средний',
            note: 'Выполнение задачи до 2 часов',
            code: 'AVERAGE'

        },
        priority: {
            id: 2,
            name: 'Средний',
            code: 'MEDIUM'
        },
        purpose: null,
        dateStart: null,
        timeStart: null,
        globalTime: '',
        deadline: null,
        reasonsForDoing: null,
        isDone: false,
        wastedTime: null,
        isSuspended: false,
        isDefault: false,
        timeSuspended: null,
        isDeleted: false,
        causeDeleted: null,

    });

    const {data: complexityNote} = useSearchComplexityNoteQuery(null);
    const {data: purpose} = useSearchPurposeQuery(null);
    const [createNote, {}] = noteApi.useCreateNoteMutation();

    return (
        <div>
            <MineFormRecord typeParent={TypeParent.purpose} create={createNote} recNotePurpose={noteDefault} record={noteLabel} complex={complexityNote}
                            shouldTo={purpose} setRecNotePurpose={setNote}/>
        </div>
    );
}

export default AddNote;


//  const dispatch = useAppDispatch();
// const {complexity, isLoading, error} = useAppSelector(state => state.complexityReducer);

/*
    useEffect(() => {
        dispatch(fetchComplexity())
    }, [])
*/