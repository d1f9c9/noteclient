import React from 'react';
import MineFormRecord from "../../components/MineFormRecord";
import {FormName, IRecordLabel, NameNote, NameOperation, ShouldTo, TypeParent} from "../../types/note";
import {useSearchComplexityNoteQuery} from "../../store/complexity/complexityNote.api";
import {useSearchPurposeQuery} from "../../store/purpose/purpose.api";
import {noteApi} from "../../store/note/note.api";
import {useLocation} from "react-router-dom";

const Note = function () {

    const location = useLocation();
    const idNote = +location.pathname.slice(6) ?? null;

    const noteLabel: IRecordLabel = {
        isDefault: 'Ежедневное задание',
        typeForm: NameOperation.note,
        formName: FormName.note,
        mainParams: 'Основные параметры',
        taskEvaluation: 'Оценка задачи',
        extendedFor: 'Продлен для',
        conditionDone: 'Условия выполнения',
        suspending: 'Приостановление выполнения',
        delete: 'Удаление задания',
        name: NameNote.purpose,
        results: 'Необходимо достичь',
        complexity: 'Уровень сложности',
        priority: 'Уровень приоритета',
        shouldTo: ShouldTo.purpose,
        timeStart: 'Время начала выполнения',
        deadline: 'Необходимо выполнить к дате',
        reasonsForDoing: 'Было достигнуто',
        isDone: 'Выполнено',
        extension: 'Задания',
        wastedTime: 'Затрачено времяни',
        isSuspended: 'Выполнение приостановлено',
        timeSuspended: 'Когда было приостановлено',
        isDeleted: 'Цель удалена',
        causeDeleted: 'Причина удаления',
    };

    const {data: complexityNote} = useSearchComplexityNoteQuery(null);
    const {data: purpose} = useSearchPurposeQuery(null);
    // @ts-ignore
    const {data: note} = noteApi.useSearchNoteByIdQuery(idNote);
    const [updateNote, {}] = noteApi.useUpdateNoteMutation();

    return (
        <div>
            <MineFormRecord typeParent={TypeParent.purpose} create={updateNote} recNotePurpose={note} record={noteLabel} complex={complexityNote}
                            shouldTo={purpose} setRecNotePurpose={() =>{}}/>
        </div>
    );
}

export default Note;




//  const dispatch = useAppDispatch();
// const {complexity, isLoading, error} = useAppSelector(state => state.complexityReducer);

/*
    useEffect(() => {
        dispatch(fetchComplexity())
    }, [])
*/