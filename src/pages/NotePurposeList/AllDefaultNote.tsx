import ListAndFilter from "../../components/ListAndFilter";
import {defaultFilterNote, INameRecordLabel, NameNote, NameOperation, ShouldTo} from "../../types/note";
import {noteApi} from "../../store/note/note.api";

const ListNote = function () {
    const noteFilter = Object.assign({}, defaultFilterNote);
    noteFilter.isDefault = true;

    const {data: note, isLoading} = noteApi.useSearchNoteDefaultQuery(null);

    const purposeLabel: INameRecordLabel = {
        name: NameNote.note,
        results: 'Необходимо достичь',
        complexity: 'Уровень сложности',
        globalTime: 'Глобальные даты',
        priority: 'Уровень приоритета',
        shouldTo: ShouldTo.purpose,
        timeStart: 'Время начала выполнения',
        deadline: 'Необходимо выполнить к дате',
        reasonsForDoing: 'Было достигнуто',
        wastedTime: 'Затрачено времяни',
    };

    return (
        <div>
            <ListAndFilter data={note} isLoading={isLoading} optionName={NameOperation.note} recordLabel={purposeLabel}/>
        </div>
    );
}

export default ListNote;