import ListAndFilter from "../../components/ListAndFilter";
import {INameRecordLabel, NameNote, NameOperation, ShouldTo} from "../../types/note";
import {useSearchNoteQuery} from "../../store/note/note.api";

const ListNote = function () {
    const {data: note, isLoading} = useSearchNoteQuery(null);

    const purposeLabel: INameRecordLabel = {
        name: NameNote.note,
        results: 'Необходимо достичь',
        complexity: 'Уровень сложности',
        priority: 'Уровень приоритета',
        shouldTo: ShouldTo.purpose,
        timeStart: 'Время начала выполнения',
        deadline: 'Необходимо выполнить к дате',
        reasonsForDoing: 'Было достигнуто',
        globalTime: 'Глобальные даты',
        wastedTime: 'Затрачено времяни',
    };

    return (
        <div>
            <ListAndFilter data={note} isLoading={isLoading} optionName={NameOperation.note} recordLabel={purposeLabel}/>
        </div>
    );
}

export default ListNote;