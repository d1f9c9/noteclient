import ListAndFilter from "../../components/ListAndFilter";
import {INameRecordLabel, NameNote, NameOperation, ShouldTo} from "../../types/note";

const ListPurpose = function () {

    const purposeLabel: INameRecordLabel = {
        action: 'Действие',
        name: NameNote.purpose,
        results: 'Необходимо достичь',
        complexity: 'Уровень сложности',
        priority: 'Уровень приоритета',
        shouldTo: ShouldTo.direction,
        timeStart: 'Время начала выполнения',
        deadline: 'Необходимо выполнить к дате',
        reasonsForDoing: 'Было достигнуто',
        globalTime: 'Глобальные даты',
        wastedTime: 'Затрачено времяни',
    }


    return (
        <div>
            <ListAndFilter isLoading={false} data={undefined} optionName={NameOperation.purpose} recordLabel={purposeLabel}/>
        </div>
    );
}

export default ListPurpose;