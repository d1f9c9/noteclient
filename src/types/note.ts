export enum NameNote {
    note = 'Задача на день',
    purpose = 'Глобальная задача'
}

export enum NameOperation {
    note = 'note',
    purpose = 'purpose'
}

export enum TypeParent {
    purpose = 'purpose',
    direction = 'direction'
}


export enum ShouldTo {
    purpose = 'Глобальная задача',
    direction = 'Направление'
}
export enum FormName {
    note = 'Форма создания задачи',
    purpose = 'Форма создания глобальной цели'
}

export interface ITypeDirection {
    id: number,
    name: string,
    code: string,
}

export interface IDirection {
    id: number,
    name: string,
    code: string,
    typeDirection: ITypeDirection[]
}

export interface IComplexity {
    id: number,
    name: string,
    note: string,
    code: string,
}

export interface IPriority {
    id: number,
    name: string,
    code: string,
}

export interface IPurpose {
    id?: number,
    name: string,
    results: string,
    complexity: IComplexity | null,
    priority: IPriority | null,
    direction: IDirection | null,
    timeStart: Date | null,
    deadline: Date | null,
    reasonsForDoing: string | null,
    isDone: boolean,
    wastedTime: string | null,
    isSuspended: boolean,
    timeSuspended: Date | null,
    isDeleted: boolean,
    causeDeleted: string | null,
    key?: number,
}

export interface INote {
    id?: number,
    name: string,
    results: string,
    complexity: IComplexity | null,
    priority: IPriority | null,
    purpose: IPurpose | null,
    globalTime: string | null,
    dateStart: string | null,
    timeStart: string | null,
    deadline: string | null,
    reasonsForDoing: string | null,
    isDone: boolean,
    wastedTime: string | null,
    isSuspended: boolean,
    timeSuspended: string | null,
    isDeleted: boolean,
    isDefault: boolean,
    causeDeleted: string | null,
}

export enum enumNameProps {
    complexity = 'complexity',
    priority = 'priority',
    purpose = 'purpose',
    direction = 'direction',
    extension = 'extension',
}


export interface IRecordLabel {
    isDefault?: 'Ежедневное задание',
    typeForm: NameOperation,
    formName: FormName,
    mainParams: 'Основные параметры',
    taskEvaluation: 'Оценка задачи',
    extendedFor: 'Продлен для',
    conditionDone: 'Условия выполнения',
    suspending: 'Приостановление выполнения',
    delete: 'Удаление задания',
    name: NameNote,
    results: 'Необходимо достичь',
    complexity: 'Уровень сложности',
    priority: 'Уровень приоритета',
    shouldTo: ShouldTo,
    timeStart: 'Время начала выполнения',
    deadline: 'Необходимо выполнить к дате',
    reasonsForDoing: 'Было достигнуто',
    isDone: 'Выполнено',
    extension: 'Задания',
    wastedTime: 'Затрачено времяни',
    isSuspended: 'Выполнение приостановлено',
    timeSuspended: 'Когда было приостановлено',
    isDeleted: 'Цель удалена',
    causeDeleted: 'Причина удаления',
}

export interface INameRecordLabel {
    action?: 'Действие'
    name: NameNote,
    results: 'Необходимо достичь',
    complexity: 'Уровень сложности',
    priority: 'Уровень приоритета',
    shouldTo: ShouldTo,
    timeStart: 'Время начала выполнения',
    deadline: 'Необходимо выполнить к дате',
    reasonsForDoing: 'Было достигнуто',
    globalTime: 'Глобальные даты',
    wastedTime: 'Затрачено времяни',
}

export interface IFilterNote {
    idPriority: number | null,
    idComplexity: number | null,
    idPurpose: number | null,
    dateStart: string | null,
    dateBeforeStart: string | null,
    dateAfterStart: string |null,
    timeStart: string | null,
    timeBeforeStart: string | null,
    timeAfterStart: string | null,
    deadline: string | null,
    beforeDeadline: string | null,
    afterDeadline: string | null,
    isDefault: boolean,
    isDone: boolean,
    isSuspended: boolean,
    isDeleted: boolean,
}

export const defaultFilterNote: IFilterNote = {
    idPriority: null,
    idComplexity: null,
    idPurpose: null,
    dateStart: null,
    dateBeforeStart: null,
    dateAfterStart: null,
    timeStart: null,
    timeBeforeStart: null,
    timeAfterStart: null,
    deadline: null,
    beforeDeadline: null,
    afterDeadline: null,
    isDefault: false,
    isDone: false,
    isSuspended: false,
    isDeleted: false,
}



