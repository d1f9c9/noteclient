export interface IUser {
    id: string | null,
    login: string | null,
    password?: string | null,
    mail: string | null,
}

export interface IRole {
    id: string,
    name: string,
    code: string,
}

export interface IPrivilege {
    id: string,
    name: string,
    code: string,
}