import React from "react";
import ListNote from "../pages/NotePurposeList/ListNote";
import ListPurpose from "../pages/NotePurposeList/ListPurpose";
import AddNote from "../pages/AddNote";
import AddPurpose from "../pages/AddPurpose";
import Note from "../pages/NotePurposeList/Note";
import AllDefaultNote from "../pages/NotePurposeList/AllDefaultNote";
import NoteToday from "../pages/NotePurposeList/NoteToday";
import NoteTomorrow from "../pages/NotePurposeList/NoteTomorrow";
import AuthForm from "../pages/AuthForm";

export interface IRoute {
    path: string,
    Element: React.ComponentType,
    exact?: boolean
}

export const privateRouters: IRoute [] = [
    {path: '/note/list', exact: true, Element: ListNote},
    {path: '/purpose/list', exact: true, Element: ListPurpose},
    {path: '/note/default', exact: true, Element: AllDefaultNote},
    {path: '/note/today', exact: true, Element: NoteToday},
    {path: '/note/tomorrow', exact: true, Element: NoteTomorrow},
    {path: '/note/add', exact: true, Element: AddNote},
    {path: '/purpose/add', exact: true, Element: AddPurpose},
    {path: '/note/:noteId', exact: true, Element: Note},
    {path: '/purpose/:purposeId', exact: true, Element: AddPurpose}
]

export const routers: IRoute[] = [
    {path: '/authorization', exact: true, Element: AuthForm},
    {path: '/registration', exact: true, Element: AuthForm},
];