import React, {useState} from 'react';
import './App.css';
import 'antd/dist/antd.css';
import {BrowserRouter} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import AppRoutes from "./components/AppRoutes";
import SideMenu from "./components/sideMenu/SideMenu";
import Button from "react-bootstrap/Button";


function App() {
    const [isAuth, setIsAuth] = useState(localStorage.getItem('isAuth'));

    function logout() {
        localStorage.setItem('isAuth', '');
        localStorage.setItem('user', '');

    }

    return (
        <BrowserRouter>
            {isAuth ? <Button onClick={() => logout()}
                              style={{position: 'absolute', left: '95%', top: '2%'}}> Выйти </Button> : <></>}
            <SideMenu/>
            <AppRoutes/>
            {/*      <ButtonMenu/>*/}
        </BrowserRouter>
    );
}

export default App;
